mod image;
mod line;
mod common;
mod obj;

use std::fs::File;
use std::io::BufReader;
use image::*;
use obj::Obj;

use std::io;

fn draw_head() -> io::Result<()> {
    let background = Pixel { a: 255, ..Default::default() };
    let mut image = Image::new(1000, 1000, background);

    let head = File::open("data/monkey.obj")?;
    let reader = BufReader::new(head);
    let obj = Obj::new(reader).unwrap();
    let white = Pixel { r: 255, g: 255, b: 255, a: 255 };

    for face in obj.faces {
        for i in 0..3 {
            let face0 = face[i];
            let face1 = face[(i+1)%3];
            let v0 = obj.verts[face0];
            let v1 = obj.verts[face1];
            let line = Line {
                start: Point {
                    x: ((v0.x + 1.) * image.width as f64 / 2.) as isize,
                    y: ((v0.y + 1.) * image.height as f64 / 2.) as isize,
                },
                end: Point {
                    x: ((v1.x + 1.) * image.width as f64 / 2.) as isize,
                    y: ((v1.y + 1.) * image.height as f64 / 2.) as isize,
                },
            };
            image.draw(&line, &white);
        }
    }
    let mut buffer = File::create("monkey.tga")?;
    Tga::encode(image, &mut buffer).unwrap();
    Ok(())
}

fn main() -> io::Result<()> {
    draw_head()
}
