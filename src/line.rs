use std::mem;
use super::common;

type Point = common::Point<isize>;
type Line = common::Line<isize>;

type Int = isize;

pub struct Bresenham {
    current: Point,
    lastx: Int,
    dx: Int,
    dy: Int,
    err: Int,
    steep: bool,
    direction: Int,
}

impl Bresenham {
    pub fn new(line: &Line) -> Self {
        let mut steep = false;
        let mut start = line.start;
        let mut end = line.end;

        if (start.x - end.x).abs() < (start.y - end.y).abs() {
            mem::swap(&mut start.x, &mut start.y);
            mem::swap(&mut end.x, &mut end.y);
            steep = true;
        }
        if start.x > end.x {
            mem::swap(&mut start, &mut end);
        }

        let dx = end.x - start.x;
        let dy = (end.y - start.y).abs();
        let direction = if end.y > start.y { 1 } else { -1 };

        Self {
            current: start,
            lastx: end.x,
            dx,
            dy,
            err: 0,
            direction,
            steep,
        }
    }
}

impl Iterator for Bresenham {
    type Item = Point;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current.x > self.lastx {
            return None;
        }
        let point = if self.steep {
            Point {
                x: self.current.y,
                y: self.current.x,
            }
        } else {
            self.current
        };
        self.current.x += 1;

        self.err += self.dy;
        if self.err * 2 > self.dx {
            self.current.y += self.direction;
            self.err -= self.dx;
        }
        Some(point)
    }
}

use std::iter::FromIterator;

impl FromIterator<Point> for Vec<(isize, isize)> {
    fn from_iter<I: IntoIterator<Item=Point>>(iter: I) -> Self {
        iter.into_iter().map(|Point {x, y}| (x,y)).collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn octant1() {
        let line = Line {
            start: Point { x: -3, y: -1 },
            end: Point { x: 3, y: 1 },
        };
        let result: Vec<(_, _)> = Bresenham::new(&line).collect();

        let expected: Vec<_> = (-3..).zip(vec![-1,-1,0,0,0,1,1]).collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn octant2() {
        let line = Line {
            start: Point { x: -1, y: -3 },
            end: Point { x: 1, y: 3 },
        };
        let result: Vec<(_, _)> = Bresenham::new(&line).collect();

        let expected: Vec<_> = vec![-1,-1,0,0,0,1,1].into_iter().zip(-3..).collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn octant3() {
        let line = Line {
            start: Point { x: -1, y: 3 },
            end: Point { x: 1, y: -3 },
        };
        let result: Vec<(_, _)> = Bresenham::new(&line).collect();

        let expected: Vec<_> = vec![1,1,0,0,0,-1,-1].into_iter().zip(-3..).collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn octant4() {
        let line = Line {
            start: Point { x: -3, y: 1 },
            end: Point { x: 3, y: -1 },
        };
        let result: Vec<(_, _)> = Bresenham::new(&line).collect();

        let expected: Vec<_> = (-3..).zip(vec![1,1,0,0,0,-1,-1]).collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn octant5() {
        let line = Line {
            start: Point { x: 3, y: 1 },
            end: Point { x: -3, y: -1 },
        };
        let result: Vec<(_, _)> = Bresenham::new(&line).collect();

        let expected: Vec<_> = (-3..).zip(vec![-1,-1,0,0,0,1,1]).collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn octant6() {
        let line = Line {
            start: Point { x: 1, y: 3 },
            end: Point { x: -1, y: -3 },
        };
        let result: Vec<(_, _)> = Bresenham::new(&line).collect();

        let expected: Vec<_> = vec![-1,-1,0,0,0,1,1].into_iter().zip(-3..).collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn octant7() {
        let line = Line {
            start: Point { x: 1, y: -3 },
            end: Point { x: -1, y: 3 },
        };
        let result: Vec<(_, _)> = Bresenham::new(&line).collect();

        let expected: Vec<_> = vec![1,1,0,0,0,-1,-1].into_iter().zip(-3..).collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn octant8() {
        let line = Line {
            start: Point { x: -3, y: 1 },
            end: Point { x: 3, y: -1 },
        };
        let result: Vec<(_, _)> = Bresenham::new(&line).collect();

        let expected: Vec<_> = (-3..).zip(vec![1,1,0,0,0,-1,-1]).collect();
        assert_eq!(result, expected);
    }
}
