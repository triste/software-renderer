use std::io;
use std::result;
use std::convert::TryFrom;

use super::common;
use super::line::*;
type Rectangle = common::Rectangle<isize>;
pub type Line = common::Line<isize>;
pub type Point = common::Point <isize>;

#[derive(Copy, Clone, Default)]
pub struct Rgba<T: Copy + Clone + Default> {
    pub r: T,
    pub g: T,
    pub b: T,
    pub a: T,
}

pub type Pixel = Rgba<u8>;

pub struct Image {
    pixels: Vec<Pixel>,
    pub width: usize,
    pub height: usize,
}

impl Image {
    #[no_mangle]
    pub fn new(width: usize, height: usize, defpix: Pixel) -> Self {
        let len = width * height * 4;
        let pixels = vec![defpix; len];
        Self { width, height, pixels }
    }
    #[no_mangle]
    pub fn draw(&mut self, line: &Line, pixel: &Pixel) {
        let botleft = Point { x: 0, y: 0 };
        let topright = Point { x: self.width as isize, y: self.height as isize};
        let rect = Rectangle::new(&botleft, &topright);
        line.clip(&rect).map(|line| {
            for Point {x,y} in Bresenham::new(&line) {
                self.pixels[y as usize * self.width + x as usize] = *pixel;
            }
        });
    }
    pub fn triangle(&mut self, points: &[Point; 3], pixel: &Pixel) {
        let mut points = points.clone();
        points.sort_by(|p1, p2| p1.y.cmp(&p2.y));
        let total_height = points[2].y - points[0].y;
        let segment_height = points[1].y - points[0].y;
        for y in points[0].y..points[1].y {
            let a = (points[2] - points[0]) * (total_height as f64 / (y - points[0].y) as f64) as isize;
            let b = (points[1] - points[0]) * (segment_height as f64 / (y - points[0].y) as f64) as isize;
            for x in a.x..b.x {
                self.pixels[y as usize * self.width + x as usize] = *pixel;
            }
        }
    }
}

#[derive(Debug)]
pub enum Error {
    TooLarge,
    Io(io::Error),
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

type Result<T> = result::Result<T, Error>;

pub trait Encoder {
    fn encode<W: io::Write>(image: Image, writer: &mut W) -> Result<()>;
}

pub struct Tga;

impl Encoder for Tga {
    fn encode<W: io::Write>(image: Image, writer: &mut W) -> Result<()> {
        let dimconv = |x| u16::try_from(x)
            .map_err(|_| Error::TooLarge)
            .map(|x| x.to_le_bytes());

        let width = dimconv(image.width)?;
        let height = dimconv(image.height)?;

        let header = vec![
            0, // id length
            0, // color map type (0: no color map)
            2, // image type (2: uncompressed true-color image)
            0, 0, 0, 0, 0, // color map specifiaction

            // image specification
            0, 0, // x-origin
            0, 0, // y-origin
            width[0], width[1],
            height[0], height[1],
            32, // pixel depth
            0, // image descriptor
        ];
        let pixels: Vec<_> = image.pixels.iter().flat_map(|p| vec![p.b, p.g, p.r, p.a]).collect();

        writer.write_all(&header)?;
        writer.write_all(&pixels)?;
        Ok(())
    }
}
