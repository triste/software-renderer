use std::io::BufReader;
use std::io::BufRead;
use std::io::Read;

#[derive(Copy, Clone)]
pub struct Vec3<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

pub struct Obj {
    pub verts: Vec<Vec3<f64>>,
    pub faces: Vec<Vec<usize>>,
}

impl Obj {
    pub fn new<R: Read>(reader: BufReader<R>) -> Result<Self, String> {
        let mut verts = Vec::new();
        let mut faces = Vec::new();

        for line in reader.lines().map(|l| l.unwrap()) {
            let mut word = line.split_ascii_whitespace();
            match word.next() {
                Some("v") => {
                    let coords: Vec<_> = word.map(|w| w.parse::<f64>().unwrap()).collect();
                    if coords.len() < 3 {
                        return Err("Wrong number of coords".to_string());
                    }
                    let v = Vec3 {
                        x: coords[0],
                        y: coords[1],
                        z: coords[2],
                    };
                    verts.push(v);
                }
                Some("f") => {
                    let mut verts: Vec<usize> = word
                        .map(|w| w.split('/')
                             .collect::<Vec<_>>()
                             .first().unwrap().parse().unwrap())
                        .collect();
                    for v in &mut verts {
                        *v -= 1;
                    }
                    faces.push(verts);
                }
                _ => continue,
            }
        }
        Ok(Self {verts, faces})
    }
}

