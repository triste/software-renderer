enum Token {
    // Vertex data
    GeomVert,
    TextVert,
    NormVert,
    PaSpVert,
    // Free-fomr curve/surface attributes
    CurveSurfType,
    Gegree,
    BasisMatrix,
    StepSize
    
    // Elements
    //
    // Polygonal
    Point,
    Line,
    Face,
    // Free-form
    Curve,
    Surface,
}
