use std::cmp;
use std::ops::*;

#[derive(Clone, Copy, Default)]
pub struct Point<T: Clone + Copy + Default> {
    pub x: T,
    pub y: T,
}

impl<T: Sub<Output = T> + Default + Copy> Sub for Point<T> {
    type Output = Self;
    fn sub(self, other: Self) -> Self::Output {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<T: Mul<Output = T> + Default + Copy> Mul<T> for Point<T> {
    type Output = Self;

    fn mul(self, scalar: T) -> Self::Output {
        Self {
            x: T::from(self.x) * scalar,
            y: T::from(self.y) * scalar,
        }
    }
}

type Outcode = u8;

enum Position {
    Inside = 0,
    Left = 1 << 0,
    Right = 1 << 1,
    Bottom = 1 << 2,
    Top = 1 << 3
}

impl<T: Ord + Copy + Clone + Default> Point<T> {
    fn outcode(&self, rect: &Rectangle<T>) -> Outcode {
        let hpos = if self.x < rect.botleft.x {
            Position::Left
        } else if self.x > rect.topright.x {
            Position::Right
        } else {
            Position::Inside
        };

        let vpos = if self.y < rect.botleft.y {
            Position::Bottom
        } else if self.y > rect.topright.y {
            Position::Top
        } else {
            Position::Inside
        };

        hpos as Outcode | vpos as Outcode
    }
}

#[derive(Clone, Copy, Default)]
pub struct Line<T: Clone + Copy + Default> {
    pub start: Point<T>,
    pub end: Point<T>,
}

impl<T: Copy + Default + Ord + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T>> Line<T> {
    pub fn clip(&self, rect: &Rectangle<T>) -> Option<Self> {
        let mut oc_start = self.start.outcode(rect);
        let mut oc_end = self.end.outcode(rect);

        let mut line = *self;
        loop {
            if oc_start | oc_end == 0 {
                break Some(line)
            } else if oc_start & oc_end != 0 {
                break None
            } else {
                let outcode = std::cmp::max(oc_start, oc_end);
                let dx = self.end.x - self.start.x;
                let dy = self.end.y - self.start.y;
                let fx = |x| self.start.y + dy * (x - self.start.x) / dx;
                let fy = |y| self.start.x + dx * (y - self.start.y) / dy;

                let point = if outcode & Position::Top as Outcode != 0 {
                    Point {
                        x: fy(rect.topright.y),
                        y: rect.topright.y,
                    }
                } else if outcode & Position::Bottom as Outcode != 0 {
                    Point {
                        x: fy(rect.botleft.y),
                        y: rect.botleft.y,
                    }
                } else if outcode & Position::Right as Outcode != 0 {
                    Point {
                        x: rect.topright.x,
                        y: fx(rect.topright.x),
                    }
                } else { // if outcode & Position::Left != 0
                    Point {
                        x: rect.botleft.x,
                        y: fx(rect.botleft.x),
                    }
                };
                if outcode == oc_start {
                    line.start = point;
                    oc_start = line.start.outcode(rect);
                } else {
                    line.end = point;
                    oc_end = line.end.outcode(rect);
                }
            }
        }
    }
}

pub struct Rectangle<T: Clone + Copy + Default> {
    botleft: Point<T>,
    topright: Point<T>,
}

impl<T: Ord + Copy + Default> Rectangle<T> {
    pub fn new(a: &Point<T>, b: &Point<T>) -> Self {
        let x = cmp::min(a.x, b.x);
        let y = cmp::min(a.y, b.y);
        let botleft = Point { x, y };

        let x = cmp::max(a.x, b.x);
        let y = cmp::max(a.y, b.y);
        let topright = Point { x, y };

        Self { botleft, topright }
    }
}
